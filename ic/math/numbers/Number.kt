package ic.math.numbers


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64


interface Number {

	val asInt32OrThrowNotSupported : Int32
	val asInt64OrThrowNotSupported : Int64

	fun negate() : Number
	fun add (other: Number) : Number
	fun multiply (other: Number) : Number

	companion object

}