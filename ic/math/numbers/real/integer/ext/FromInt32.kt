package ic.math.numbers.real.integer.ext


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64

import ic.math.numbers.real.integer.Integer
import ic.math.numbers.real.integer.IntegerFromInt64


fun Integer.Companion.fromInt32 (int32: Int32) : Integer = IntegerFromInt64(int32.asInt64)