package ic.math.numbers.real.integer.ext


import ic.base.primitives.int64.Int64
import ic.base.throwables.NotSupportedException

import ic.math.numbers.real.integer.Integer


inline val Integer.asInt64 : Int64 get() {
	try {
		return asInt64OrThrowNotSupported
	} catch (t: NotSupportedException) {
		throw NotSupportedException.Runtime("this: $this")
	}
}


inline val Integer.asInt64OrNull : Int64? get() {
	try {
		return asInt64OrThrowNotSupported
	} catch (t: NotSupportedException) {
		return null
	}
}