package ic.math.numbers.real.integer.ext


import ic.base.primitives.int64.Int64

import ic.math.numbers.real.integer.Integer
import ic.math.numbers.real.integer.IntegerFromInt64


fun Integer.Companion.fromInt64 (int64: Int64) : Integer = IntegerFromInt64(int64)