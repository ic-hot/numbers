package ic.math.numbers.real.integer.ext


import ic.base.primitives.int32.Int32
import ic.base.throwables.NotSupportedException

import ic.math.numbers.real.integer.Integer


inline val Integer.asInt32 : Int32 get() {
	try {
		return asInt32OrThrowNotSupported
	} catch (t: NotSupportedException) {
		throw NotSupportedException.Runtime("this: $this")
	}
}


inline val Integer.asInt32OrNull : Int32? get() {
	try {
		return asInt32OrThrowNotSupported
	} catch (t: NotSupportedException) {
		return null
	}
}