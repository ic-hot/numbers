package ic.math.numbers.real.integer


import ic.math.numbers.real.RealNumber


interface Integer : RealNumber {

	override fun negate() : Integer

	companion object {

		val Zero : Integer = IntegerFromInt64(0)
		val One  : Integer = IntegerFromInt64(1)

	}

}