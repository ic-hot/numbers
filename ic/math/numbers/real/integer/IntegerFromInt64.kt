package ic.math.numbers.real.integer


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.base.reflect.ext.className

import ic.math.numbers.Number
import ic.math.numbers.real.RealNumber


internal data class IntegerFromInt64 (@JvmField val int64Value: Int64) : Integer {


	override val asInt32OrThrowNotSupported get() = int64Value.asInt32

	override val asInt64OrThrowNotSupported get() = int64Value


	override fun negate() : Integer {
		if (int64Value == Int64.MIN_VALUE) throw NotImplementedError()
		return IntegerFromInt64(-int64Value)
	}

	override fun add (other: Number) : Number {
		if (other is IntegerFromInt64) {
			return IntegerFromInt64(this.int64Value + other.int64Value)
		} else {
			return other.add(this)
		}
	}

	override fun multiply (other: Number) : Number {
		if (other is IntegerFromInt64) {
			return IntegerFromInt64(this.int64Value * other.int64Value)
		} else {
			return other.multiply(this)
		}
	}


	override fun compareTo (other: RealNumber) : Int32 {
		if (other is IntegerFromInt64) {
			return int64Value.compareTo(other.int64Value)
		} else {
			throw NotImplementedError("other.className: ${ other.className }")
		}
	}


	override fun toString() = int64Value.toString()


}