package ic.math.numbers.real


import ic.base.primitives.int32.Int32

import ic.math.numbers.Number


interface RealNumber : Number {

	override fun negate() : RealNumber

	operator fun compareTo (other: RealNumber) : Int32

}