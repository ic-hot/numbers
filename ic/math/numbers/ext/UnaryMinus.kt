@file:Suppress("NOTHING_TO_INLINE")


package ic.math.numbers.ext


import ic.math.numbers.Number


inline operator fun Number.unaryMinus() = negate()