package ic.math.numbers.ext


import ic.base.strings.ext.parse.parseInt64

import ic.math.numbers.Number
import ic.math.numbers.real.integer.IntegerFromInt64


fun Number.Companion.parse (string: String) : Number {

	return IntegerFromInt64(
		string.parseInt64()
	)

}