package ic.math.numbers.funs


import ic.struct.collection.Collection

import ic.math.numbers.Number
import ic.math.numbers.ext.times
import ic.math.numbers.real.integer.Integer.Companion.One
import ic.struct.collection.ext.foreach.breakableForEach


fun product (numbers: Collection<Number>) : Number {
	var product : Number = One
	numbers.breakableForEach { product *= it }
	return product
}