package ic.math.numbers.funs


import ic.struct.collection.Collection
import ic.struct.collection.ext.foreach.breakableForEach

import ic.math.numbers.Number
import ic.math.numbers.ext.plus
import ic.math.numbers.real.integer.Integer.Companion.Zero


fun sum (numbers: Collection<Number>) : Number {
	var sum : Number = Zero
	numbers.breakableForEach { sum += it }
	return sum
}