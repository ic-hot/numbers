package ic.base.primitives.int32.ext


import ic.base.primitives.int32.Int32

import ic.math.numbers.real.integer.Integer
import ic.math.numbers.real.integer.IntegerFromInt64


val Int32.asNumber : Integer get() = IntegerFromInt64(this.asInt64)