package ic.base.primitives.int64.ext


import ic.base.primitives.int64.Int64

import ic.math.numbers.real.integer.Integer
import ic.math.numbers.real.integer.IntegerFromInt64


val Int64.asNumber : Integer get() = IntegerFromInt64(this)